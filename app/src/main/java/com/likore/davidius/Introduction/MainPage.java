package com.likore.davidius.Introduction;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;

import androidx.appcompat.app.AppCompatActivity;

//import com.appsflyer.AppsFlyerLib;
//import com.facebook.FacebookSdk;
//import com.facebook.applinks.AppLinkData;
import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;
import com.onesignal.OneSignal;

import java.util.Map;

import im.delight.android.webview.AdvancedWebView;
import com.likore.davidius.R;

public class MainPage extends AppCompatActivity implements AdvancedWebView.Listener {
    private static final String AF_DEV_KEY = Constants.appsFlyerKey;
    private final String ONESIGNAL_APP_ID = Constants.oneSignalAppId;
    private final String moderationUrl = Constants.moderationUrl;
    private final boolean debug = Constants.debugMode;
    private String urlToGo = Constants.mainUrl;
    private final String APP_PREFERENCES = "appsettings";
    private final String APP_PREFERENCES_URL = "url";
    private SharedPreferences mSettings;
    private ModerationCheck httpURLConnection;
    private AdvancedWebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // removeTopStatusBar() should be before super();
        removeTopStatusBar();
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_web);
        hideTopBar();

        mWebView = findViewById(R.id.webview);
        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        if (debug) {
            mSettings.edit().clear().apply();
        }

        if (mSettings.contains("isBot")) {
            loadNewsApp();
            return;
        }

        if (getAndSetLastAppUrl()) {
            loadMainApp(urlToGo);
            return;
        }

        if (isOnline()) {
            appFlyerInitStart();
            oneSignalInitStart();

            int serverResponse = sendRequest(moderationUrl, "mod");
            if (serverResponse == 200) {
                loadMainApp(urlToGo + "/?key=" + Constants.appName);
            } else {
                loadNewsApp();
            }
        } else {
            showNoConnectionPage();
        }
    }

    private Integer sendRequest(String url, String tag) {
        httpURLConnection = new ModerationCheck(url);
        httpURLConnection.start();

        try {
            httpURLConnection.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        int response = httpURLConnection.serverResponse();
        Log.d(tag, "response code is:" + response);
        return response;
    }

    private void loadMainApp(String url) {
        mWebView.post(new Runnable() {
            @Override
            public void run() {
                mWebView.addJavascriptInterface(new checkHTMLString(), "HTMLOUT");
                mWebView.setListener(MainPage.this, MainPage.this);
                mWebView.setMixedContentAllowed(false);
                //        if (debug) {
                //            WebView.setWebContentsDebuggingEnabled(true);
                //        }
                mWebView.loadUrl(url);
            }
        });
    }

    private void loadNewsApp() {
        Intent intent = new Intent(this, IntroductionMain.class);
        startActivity(intent);
    }

    private void showNoConnectionPage() {
        Intent intent = new Intent(this, NoConnectionActivity.class);
        startActivity(intent);
    }

    private Boolean getAndSetLastAppUrl() {
        if (mSettings.contains(APP_PREFERENCES_URL)) {
            urlToGo = mSettings.getString(APP_PREFERENCES_URL, Constants.mainUrl);
            return true;
        }

        return false;
    }

    private boolean isOnline() {
        Context context = this;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    private void hideTopBar() {
        try {
            this.getSupportActionBar().hide();
        }
        catch (NullPointerException ignored){}
    }

    private void removeTopStatusBar() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    class checkHTMLString {
        @JavascriptInterface
        public void showHTML(String html_data) {
            saveSettings("isBot", "true");
            loadNewsApp();
        }
    }

    @Override
    public void onPageFinished(String url) {
        urlToGo = mWebView.getUrl();
        saveSettings(APP_PREFERENCES_URL, urlToGo);
    }

    private void saveSettings(String key, String value) {
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(key, value);
        editor.apply();
    }

    private void appFlyerInitStart() {
        AppsFlyerConversionListener conversionListener = new AppsFlyerConversionListener() {

            @Override
            public void onConversionDataSuccess(Map<String, Object> conversionData) {

                for (String attrName : conversionData.keySet()) {
                    Log.d("LOG_TAG", "attribute: " + attrName + " = " + conversionData.get(attrName));
                }
            }

            @Override
            public void onConversionDataFail(String errorMessage) {
                Log.d("LOG_TAG", "error getting conversion data: " + errorMessage);
            }

            @Override
            public void onAppOpenAttribution(Map<String, String> attributionData) {

                for (String attrName : attributionData.keySet()) {
                    Log.d("LOG_TAG", "attribute: " + attrName + " = " + attributionData.get(attrName));
                }

            }

            @Override
            public void onAttributionFailure(String errorMessage) {
                Log.d("LOG_TAG", "error onAttributionFailure : " + errorMessage);
            }
        };

        AppsFlyerLib.getInstance().init(AF_DEV_KEY, conversionListener, this);
        AppsFlyerLib.getInstance().start(this);
    }

    private void oneSignalInitStart() {
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);
        // OneSignal Initialization
        OneSignal.initWithContext(this);
        OneSignal.setAppId(ONESIGNAL_APP_ID);
    }

    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();
        mWebView.onResume();
        // ...
    }

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        mWebView.onPause();
        // ...
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mWebView.onDestroy();
        // ...
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        mWebView.onActivityResult(requestCode, resultCode, intent);
        // ...
    }

    @Override
    public void onBackPressed() {
        if (!mWebView.onBackPressed()) { return; }
        // ...
        super.onBackPressed();
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) { }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) { }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) { }

    @Override
    public void onExternalPageRequest(String url) { }
}
