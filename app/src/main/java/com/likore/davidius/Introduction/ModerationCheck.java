package com.likore.davidius.Introduction;

import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class ModerationCheck extends Thread {
    private String urlGo = "";
    private int responseCode;

    public ModerationCheck(String url) {
        super();
        urlGo = url;
    }

    @Override
    public void run() {
        HttpURLConnection urlConnection = null;

        try {
            URL myUrl = new URL(urlGo);
            urlConnection = (HttpURLConnection) myUrl.openConnection();
            Log.d("Request type", urlConnection.getRequestMethod());
            responseCode = urlConnection.getResponseCode();
            Log.d("Response code", String.valueOf(responseCode));
        } catch (IOException e) {
            Log.e("Response", "Error during response");
            e.printStackTrace();
            responseCode = 404;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
                Log.d("Response:", "finished");
            }
        }
    }

    public int serverResponse () {
        return responseCode;
    }

}
