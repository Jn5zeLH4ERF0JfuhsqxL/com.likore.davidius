package com.likore.davidius.Introduction;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.likore.davidius.R;

public class NoConnectionActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        removeTopStatusBar();
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_no_connection);
        hideTopBar();

        final Context context = this;
        Button clickButton = findViewById(R.id.reload_connection);

        clickButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(context, MainPage.class);
                startActivity(intent);
            }
        });
    }

    private void hideTopBar() {
        try {
            this.getSupportActionBar().hide();
        } catch (NullPointerException ignored){}
    }

    private void removeTopStatusBar() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}
